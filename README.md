# NeoMetSys

## Installation du projet

### Lancer docker
> docker-compose up --build

### Installer la base de données
> docker exec -i neometsys_db_1 mysql -uwordpress -pwordpress wordpress < dump-sql/neometsys.sql

## Changer le domaine dans le site
> docker-compose exec wordpress bash

### Pour vérifier le changement
> wp search-replace http://localhost/neometsys http://0.0.0.0:8000 --dry-run --allow-root
### Pour faire le changement
> wp search-replace http://localhost/neometsys http://0.0.0.0:8000 --allow-root

### Ajuster les droits
> chown -R www-data:www-data wp-content/

### Récupérer les données de production
Utiliser la commande scp pour récupérer le dump sql.

Certains uploads seront manquants.

### Accès au site
http://0.0.0.0:8000
