<?php 
	
	add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
	
	function theme_enqueue_styles() {
		wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
	}

	// Display current year 
	function year_shortcode() {
		$year = date_i18n('Y');
		return $year;
	}
	add_shortcode('year', 'year_shortcode');
